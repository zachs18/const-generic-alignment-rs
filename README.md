# `const-generic-alignment`

This crate provides an `AlignedZst<N>` type which is a zero-sized type with the specified alignment.
